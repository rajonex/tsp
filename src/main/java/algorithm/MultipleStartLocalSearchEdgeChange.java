package algorithm;

import factory.LocalSearchFactory;
import result.ResultGraphs;
import utils.ResultCalculation;
import utils.TSPUtils;

import java.util.List;

public class MultipleStartLocalSearchEdgeChange implements Algorithm {

    private Algorithm baseGraphGenerator;
    private int numberOfIterations;
    private boolean localSearchType;

    public MultipleStartLocalSearchEdgeChange(boolean localSearchType, Algorithm baseGraphGenerator, int numberOfIterations) {
        this.baseGraphGenerator = baseGraphGenerator;
        this.numberOfIterations = numberOfIterations;
        this.localSearchType = localSearchType;
    }

    @Override
    public ResultGraphs calculate(int sizeGraph, long[][] costMatrix) {
        ResultGraphs returnResultGraphs = null;
        ResultCalculation resultCalculation = new ResultCalculation(costMatrix);
        long bestCost = Long.MAX_VALUE;
        for(int i=0; i<numberOfIterations; i++){
            ResultGraphs resultGraphsBeforeWrap = baseGraphGenerator.calculate(sizeGraph, costMatrix);
            Algorithm localSearch = LocalSearchFactory.createLocalSearchAlgorithm(localSearchType, resultGraphsBeforeWrap.getFirstGraph(), resultGraphsBeforeWrap.getSecondGraph());
            ResultGraphs resultGraphs = localSearch.calculate(sizeGraph, costMatrix);
            long iterationCost = resultCalculation.calculateWholeDistanceOfTwoGraphs(resultGraphs.getFirstGraph(), resultGraphs.getSecondGraph());
            if(iterationCost < bestCost){
                bestCost = iterationCost;
                returnResultGraphs = resultGraphs;
            }
        }
        return returnResultGraphs;
    }
}
