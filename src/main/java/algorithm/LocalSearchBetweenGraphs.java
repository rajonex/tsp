package algorithm;

import result.ResultGraphs;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class LocalSearchBetweenGraphs extends LocalSearch {


    private List<Integer> firstGraph;
    private List<Integer> secondGraph;

    public LocalSearchBetweenGraphs(List<Integer> firstGraph, List<Integer> secondGraph) {
        this.firstGraph = new LinkedList<>(firstGraph);
        this.secondGraph = new LinkedList<>(secondGraph);
    }

    public LocalSearchBetweenGraphs(ResultGraphs resultGraphs){
        this.firstGraph = new LinkedList<>(resultGraphs.getFirstGraph());
        this.secondGraph = new LinkedList<>(resultGraphs.getSecondGraph());
    }

    @Override
    public ResultGraphs calculate(int sizeGraph, long[][] costMatrix) {
        ResultGraphs resultGraphsAfterFirstPart = calculationBetweenGraphs(firstGraph, secondGraph, costMatrix, sizeGraph);
        firstGraph = resultGraphsAfterFirstPart.getFirstGraph();
        secondGraph = resultGraphsAfterFirstPart.getSecondGraph();

        return calculationForEachGraph(firstGraph, secondGraph, costMatrix, sizeGraph);
    }

    private ResultGraphs calculationBetweenGraphs(List<Integer> firstGraph, List<Integer> secondGraph, long[][] costMatrix, int sizeGraph){
        long profit;
        for(int i=0; i<sizeGraph/2; i++){
            long graphProfit = 0;
            int firstPoint = -1;
            int secondPoint = -1;
            for(int j=0; j<sizeGraph/2; j++){
                profit = calculateProfitOfChangeInTwoGraphs(i, j, firstGraph, secondGraph, costMatrix);
                if(profit > graphProfit){
                    graphProfit = profit;
                    firstPoint = i;
                    secondPoint = j;
                }
            }

            if(firstPoint != -1 && secondPoint != -1){
                swapPoints(firstPoint, secondPoint, firstGraph, secondGraph);
            }
        }

        for(int i=0; i<sizeGraph/2; i++){
            long graphProfit = 0;
            int firstPoint = -1;
            int secondPoint = -1;
            for(int j=0; j<sizeGraph/2; j++){
                profit = calculateProfitOfChangeInTwoGraphs(i, j, secondGraph, firstGraph, costMatrix);
                if(profit > graphProfit){
                    graphProfit = profit;
                    firstPoint = i;
                    secondPoint = j;
                }
            }

            if(firstPoint != -1 && secondPoint != -1){
                swapPoints(firstPoint, secondPoint, secondGraph, firstGraph);
            }
        }

        return new ResultGraphs(firstGraph, secondGraph);
    }




    private long calculateProfitOfChangeInTwoGraphs(int firstPoint, int secondPoint, List<Integer> firstGraph, List<Integer> secondGraph, long[][] costMatrix){
        int firstPointMinusOne = firstPoint-1;
        if(firstPointMinusOne < 0)
            firstPointMinusOne = firstGraph.size()-1;
        int firstPointMinusOneValue = firstGraph.get(firstPointMinusOne);
        int firstPointValue = firstGraph.get(firstPoint);
        int firstPointPlusOne = firstPoint+1;
        if(firstPointPlusOne >= firstGraph.size())
            firstPointPlusOne = 0;
        int firstPointPlusOneValue = firstGraph.get(firstPointPlusOne);

        int secondPointMinusOne = secondPoint-1;
        if(secondPointMinusOne < 0)
            secondPointMinusOne = secondGraph.size()-1;
        int secondPointMinusOneValue = secondGraph.get(secondPointMinusOne);
        int secondPointValue = secondGraph.get(secondPoint);
        int secondPointPlusOne = secondPoint+1;
        if(secondPointPlusOne >= secondGraph.size())
            secondPointPlusOne = 0;
        int secondPointPlusOneValue = secondGraph.get(secondPointPlusOne);

        return (costMatrix[firstPointMinusOneValue][firstPointValue] + costMatrix[firstPointValue][firstPointPlusOneValue]
                - costMatrix[firstPointMinusOneValue][secondPointValue] - costMatrix[secondPointValue][firstPointPlusOneValue]
                + costMatrix[secondPointMinusOneValue][secondPointValue] + costMatrix[secondPointValue][secondPointPlusOneValue]
                - costMatrix[secondPointMinusOneValue][firstPointValue] - costMatrix[firstPointValue][secondPointPlusOneValue]
        );
    }



    private void swapPoints(int firstPoint, int secondPoint, List<Integer> firstGraph, List<Integer> secondGraph){
        int temp = firstGraph.get(firstPoint);
        firstGraph.set(firstPoint, secondGraph.get(secondPoint));
        secondGraph.set(secondPoint, temp);
    }
}
