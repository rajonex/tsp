package algorithm;

import result.ResultGraphs;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class NearestNeighbor implements Algorithm {
//    long[][] costMatrix;

    public NearestNeighbor() {
    }

    public ResultGraphs calculate(int sizeGraph, long[][] costMatrix){
        List<Integer> availablePoints = new LinkedList<>();
        for(int i = 0; i < sizeGraph; i++){
            availablePoints.add(i);
        }

        List<Integer> firstGraph = new LinkedList<>();
        List<Integer> secondGraph = new LinkedList<>();

        int firstNoElements = 1;
        int secondNoElements = 1;

//        Random rand = new Random(111);
        Random rand = new Random();

        int lastNoFirstGraph = rand.nextInt(sizeGraph);
        int lastNoSecondGraph = rand.nextInt(sizeGraph);
        while(lastNoFirstGraph == lastNoSecondGraph){
            lastNoSecondGraph = rand.nextInt(sizeGraph);
        }

        firstGraph.add(lastNoFirstGraph);
        secondGraph.add(lastNoSecondGraph);
        availablePoints.remove(Integer.valueOf(lastNoFirstGraph));
        availablePoints.remove(Integer.valueOf(lastNoSecondGraph));

        while(firstNoElements + secondNoElements < sizeGraph){
            int firstPoint = -1;
            if(firstNoElements < sizeGraph/2)
                firstPoint = minDistancePoint(lastNoFirstGraph, costMatrix[lastNoFirstGraph], availablePoints);

            int secondPoint = -1;
            if(secondNoElements < sizeGraph/2)
                secondPoint = minDistancePoint(lastNoSecondGraph, costMatrix[lastNoSecondGraph], availablePoints);

            if(firstPoint == -1) {
                if(secondPoint == -1){
                    throw new IllegalStateException();
                }else{
                    secondGraph.add(secondPoint);
                    secondNoElements++;
                    availablePoints.remove(Integer.valueOf(secondPoint));
                    lastNoSecondGraph = secondPoint;
                }
            } else{
                if(secondPoint == -1){
                    firstGraph.add(firstPoint);
                    firstNoElements++;
                    availablePoints.remove(Integer.valueOf(firstPoint));
                    lastNoFirstGraph = firstPoint;
                }else{
                    if(costMatrix[lastNoFirstGraph][firstPoint] < costMatrix[lastNoSecondGraph][secondPoint]){
                        firstGraph.add(firstPoint);
                        firstNoElements++;
                        availablePoints.remove(Integer.valueOf(firstPoint));
                        lastNoFirstGraph = firstPoint;
                    }else{
                        secondGraph.add(secondPoint);
                        secondNoElements++;
                        availablePoints.remove(Integer.valueOf(secondPoint));
                        lastNoSecondGraph = secondPoint;
                    }
                }
            }
//
//
//            if(costMatrix[lastNoFirstGraph][firstPoint] < costMatrix[lastNoSecondGraph][secondPoint] && firstPoint != -1){
//                firstGraph.add(firstPoint);
//                firstNoElements++;
//                availablePoints.remove(Integer.valueOf(firstPoint));
//                lastNoFirstGraph = firstPoint;
//            } else{
//                if(secondPoint != -1){
//                    secondGraph.add(secondPoint);
//                    secondNoElements++;
//                    availablePoints.remove(Integer.valueOf(secondPoint));
//                    lastNoSecondGraph = secondPoint;
//                } else{
//                    throw new IllegalStateException();
//                }
//            }
//



        }

        return new ResultGraphs(firstGraph, secondGraph);
    }

    private int minDistancePoint(int referencePoint, long[] costVector, List<Integer> availablePoints){
        long minValue = Long.MAX_VALUE;
        int nearestPoint = -1;
        for(int availablePoint: availablePoints){
            if(costVector[availablePoint] < minValue && referencePoint != availablePoint){
                minValue = costVector[availablePoint];
                nearestPoint = availablePoint;
            }
        }
        return nearestPoint;
    }

}
