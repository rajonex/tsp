package algorithm;

import factory.LocalSearchFactory;
import result.ResultGraphs;
import utils.ResultCalculation;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class IterativeLocalSearchEdgeChange implements Algorithm {
    private List<Integer> firstGraph;
    private List<Integer> secondGraph;
    boolean localSearchType;
    private Duration timeLimit;

    public IterativeLocalSearchEdgeChange(boolean localSearchType, List<Integer> firstGraph, List<Integer> secondGraph, Duration timeLimit) {
        this.firstGraph = new LinkedList<>(firstGraph);
        this.secondGraph = new LinkedList<>(secondGraph);
        this.timeLimit = timeLimit;
        this.localSearchType = localSearchType;
    }



    @Override
    public ResultGraphs calculate(int sizeGraph, long[][] costMatrix) {
        ResultCalculation resultCalculation = new ResultCalculation(costMatrix);
        Random rand = new Random();
        Instant start = Instant.now();
        Algorithm localSearch = LocalSearchFactory.createLocalSearchAlgorithm(localSearchType, firstGraph, secondGraph);
        ResultGraphs resultGraphs = localSearch.calculate(sizeGraph, costMatrix);
        firstGraph = resultGraphs.getFirstGraph();
        secondGraph = resultGraphs.getSecondGraph();
        List<Integer> bestFirstGraph = new LinkedList<>(firstGraph);
        List<Integer> bestSecondGraph = new LinkedList<>(secondGraph);
        long nowBestDistance = resultCalculation.calculateWholeDistanceOfTwoGraphs(bestFirstGraph, bestSecondGraph);

        while(Duration.between(start, Instant.now()).compareTo(timeLimit) < 0){
            perturbation(sizeGraph, costMatrix, rand);
            localSearch = LocalSearchFactory.createLocalSearchAlgorithm(localSearchType, firstGraph, secondGraph);
            resultGraphs = localSearch.calculate(sizeGraph, costMatrix);
            firstGraph = resultGraphs.getFirstGraph();
            secondGraph = resultGraphs.getSecondGraph();
//            System.out.println(resultCalculation.calculateWholeDistanceOfTwoGraphs(firstGraph, secondGraph));
            if(nowBestDistance > resultCalculation.calculateWholeDistanceOfTwoGraphs(firstGraph, secondGraph)){
                bestFirstGraph = new LinkedList<>(firstGraph);
                bestSecondGraph = new LinkedList<>(secondGraph);
                nowBestDistance = resultCalculation.calculateWholeDistanceOfTwoGraphs(firstGraph, secondGraph);
            }
        }

        return new ResultGraphs(bestFirstGraph, bestSecondGraph);
    }

    private void perturbation(int sizeGraph, long[][] costMatrix, Random rand){
        int indexOfFirstGraphMaxPoint = -1;
        int indexOfSecondGraphMaxPoint = -1;

        long costOfMaxPointFirstGraph = 0L;
        long costOfMaxPointSecondGraph = 0L;


        for(int i=0; i<sizeGraph/2; i++) {
            long costOfIterFirstGraph = 0;
            long costOfIterSecondGraph = 0;
            int iterPointFirstGraph = firstGraph.get(i);
            int iterPointSecondGraph = secondGraph.get(i);

            for(int j=0; j<sizeGraph/2; j++){
                costOfIterFirstGraph += costMatrix[iterPointFirstGraph][firstGraph.get(j)];
                costOfIterSecondGraph += costMatrix[iterPointSecondGraph][secondGraph.get(j)];
            }

            if(costOfIterFirstGraph > costOfMaxPointFirstGraph){
                costOfMaxPointFirstGraph = costOfIterFirstGraph;
                indexOfFirstGraphMaxPoint = i;
            }

            if(costOfIterSecondGraph > costOfMaxPointSecondGraph){
                costOfMaxPointSecondGraph = costOfIterSecondGraph;
                indexOfSecondGraphMaxPoint = i;
            }
        }

        int temp = firstGraph.get(indexOfFirstGraphMaxPoint);
        firstGraph.set(indexOfFirstGraphMaxPoint, secondGraph.get(indexOfSecondGraphMaxPoint));
        secondGraph.set(indexOfSecondGraphMaxPoint, temp);
//
//        for(int i=0; i<3; i++) {
//            int indexFirstGraph = rand.nextInt(sizeGraph / 2);
//            int indexSecondGraph = rand.nextInt(sizeGraph / 2);
////            int temp = firstGraph.get(indexFirstGraph);
////            firstGraph.set(indexFirstGraph, secondGraph.get(indexSecondGraph));
////            secondGraph.set(indexSecondGraph, temp);
//            Collections.swap(secondGraph, indexFirstGraph, indexSecondGraph);
//        }
    }
}
