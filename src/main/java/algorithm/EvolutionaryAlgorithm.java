package algorithm;

import factory.LocalSearchFactory;
import result.ResultGraphs;
import result.ResultPopulation;
import utils.ResultCalculation;

import java.time.Duration;
import java.time.Instant;
import java.util.*;

public class EvolutionaryAlgorithm implements Algorithm {

    private int populationSize;
    private Algorithm createStartSolutionsAlgorithm;
    private Duration timeLimit;
    boolean localSearchType;

    public EvolutionaryAlgorithm(boolean localSearchType, Algorithm createStartSolutionsAlgorithm, int populationSize, Duration timeLimit) {
        this.createStartSolutionsAlgorithm = createStartSolutionsAlgorithm;
        this.populationSize = populationSize;
        this.timeLimit = timeLimit;
        this.localSearchType = localSearchType;
    }

    @Override
    public ResultGraphs calculate(int sizeGraph, long[][] costMatrix) {
        Instant start = Instant.now();
        List<ResultGraphs> population = new LinkedList<>();
        Random random = new Random();
        List<Long> populationCosts = new LinkedList<>();
        ResultCalculation resultCalculation = new ResultCalculation(costMatrix);


        for(int i=0; i<populationSize; i++){
            ResultGraphs parent = createStartSolutionsAlgorithm.calculate(sizeGraph, costMatrix);
            population.add(parent);
            populationCosts.add(resultCalculation.calculateWholeDistanceOfTwoGraphs(parent));
        }

        int bestIndex = populationCosts.indexOf(Collections.min(populationCosts));
        long bestCost = populationCosts.get(bestIndex);
        ResultGraphs bestResultGraphs = population.get(bestIndex);

        int i = 1;

        while(Duration.between(start, Instant.now()).compareTo(timeLimit) < 0){
            List<ResultGraphs> childs = crossingAndLocalSearch(population, random, costMatrix, sizeGraph);

            population.addAll(childs);
            for(ResultGraphs child: childs){
                populationCosts.add(resultCalculation.calculateWholeDistanceOfTwoGraphs(child));
            }

            ResultPopulation resultPopulation = tournamentSelection(population, populationCosts, random, populationSize);
            population = resultPopulation.getPopulation();
            populationCosts = resultPopulation.getPopulationCosts();

            for(int j=0; j<populationSize; j++){
                if(bestCost > populationCosts.get(j)){
                    bestCost = populationCosts.get(j);
                    bestResultGraphs = population.get(j);
                }
            }

            System.out.println("Wykonalo sie: " + (++i));
        }

        return bestResultGraphs;
    }

    private List<ResultGraphs> crossingAndLocalSearch(List<ResultGraphs> parents, Random random, long[][] costMatrix, int sizeGraph){
        List<ResultGraphs> copyParents = new LinkedList<>(parents);
        List<ResultGraphs> resultChilds = new LinkedList<>();
        List<Integer> availablePoints = new LinkedList<>();
        while(copyParents.size() > 1){
            int firstParentIndex = random.nextInt(copyParents.size());
            ResultGraphs firstParent = copyParents.get(firstParentIndex);
            copyParents.remove(firstParentIndex);

            int secondParentIndex = random.nextInt(copyParents.size());
            ResultGraphs secondParent = copyParents.get(secondParentIndex);
            copyParents.remove(secondParentIndex);

            availablePoints.addAll(secondParent.getFirstGraph());
            availablePoints.addAll(secondParent.getSecondGraph());

            List<Integer> childFirstGraph = new LinkedList<>();
            List<Integer> childSecondGraph = new LinkedList<>();

            List<Integer> partOfFirstParentFirstGraph = firstParent.getFirstGraph().subList(0, sizeGraph/4);
            childFirstGraph.addAll(partOfFirstParentFirstGraph);
            availablePoints.removeAll(partOfFirstParentFirstGraph);

            List<Integer> partOfFirstParentSecondGraph = firstParent.getSecondGraph().subList(0, sizeGraph/4);
            childSecondGraph.addAll(partOfFirstParentSecondGraph);
            availablePoints.removeAll(partOfFirstParentSecondGraph);

            for(Integer point: availablePoints){
                if(childFirstGraph.size() < sizeGraph/2){
                    childFirstGraph.add(point);
                } else{
                    childSecondGraph.add(point);
                }
            }
//            Iterator<Integer> availablePointsIterator = availablePoints.iterator();
//            while(availablePointsIterator.hasNext()){
//                Integer point = availablePointsIterator.next();
//                ava
//            }
            Algorithm localSearch = LocalSearchFactory.createLocalSearchAlgorithm(localSearchType, childFirstGraph, childSecondGraph);
            ResultGraphs resultGraphs = localSearch.calculate(sizeGraph, costMatrix);

            resultChilds.add(resultGraphs);
        }

        return resultChilds;
    }

    private ResultPopulation tournamentSelection(List<ResultGraphs> population, List<Long> populationCosts, Random random, int populationSize){
        List<ResultGraphs> resultPopulation = new LinkedList<>();
        List<Long> resultPopulationCosts = new LinkedList<>();

        for(int i=0; i<populationSize; i++){
            int bestIndex = random.nextInt(populationSize);
            long bestCost = populationCosts.get(bestIndex);
            for(int j=0; j<populationSize/5; j++){
                int index = random.nextInt(populationSize);
                long indexCost = populationCosts.get(index);
                if(bestCost > indexCost)
                {
                    bestCost = indexCost;
                    bestIndex = index;
                }
            }

            resultPopulation.add(population.get(bestIndex));
            resultPopulationCosts.add(bestCost);
        }

        return new ResultPopulation(resultPopulation, resultPopulationCosts);
    }

}
