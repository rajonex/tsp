package algorithm;

import result.ResultGraphs;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class RandTSP implements Algorithm {
    @Override
    public ResultGraphs calculate(int sizeGraph, long[][] costMatrix) {
        List<Integer> availablePoints = new LinkedList<>();
        for(int i = 0; i < sizeGraph; i++){
            availablePoints.add(i);
        }

        List<Integer> firstGraph = new LinkedList<>();
        List<Integer> secondGraph = new LinkedList<>();

        Random rand = new Random();
        for(int i=sizeGraph/2; i>0; i--){
            int firstGraphElement = availablePoints.get(rand.nextInt(2*i));
            addElementToGraph(firstGraphElement, firstGraph, availablePoints);
            int secondGraphElement = availablePoints.get(rand.nextInt(2*i-1));
            addElementToGraph(secondGraphElement, secondGraph, availablePoints);
        }

        return new ResultGraphs(firstGraph, secondGraph);
    }

    private void addElementToGraph(int element, List<Integer> graph, List<Integer> availablePoints){
        graph.add(element);
        availablePoints.remove(Integer.valueOf(element));
    }
}
