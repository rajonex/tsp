package algorithm;

import result.ResultGraphs;
import result.ResultGreedyCyclePoint;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class GreedyCycle implements Algorithm {


    @Override
    public ResultGraphs calculate(int sizeGraph, long[][] costMatrix) {
        List<Integer> availablePoints = new LinkedList<>();
        for(int i = 0; i < sizeGraph; i++){
            availablePoints.add(i);
        }

        int firstNoElements = 3;
        int secondNoElements = 3;

//        Random rand = new Random(111);
        Random rand = new Random();

        int firstPointInFirstGraph = rand.nextInt(sizeGraph);
        List<Integer> firstGraph = createThreePointsGraph(firstPointInFirstGraph, costMatrix, availablePoints);

        int firstPointInSecondGraph = rand.nextInt(sizeGraph);
        while(!availablePoints.contains(Integer.valueOf(firstPointInSecondGraph))){
            firstPointInSecondGraph = rand.nextInt(sizeGraph);
        }
        List<Integer> secondGraph = createThreePointsGraph(firstPointInSecondGraph, costMatrix, availablePoints);


        while(firstNoElements + secondNoElements < sizeGraph) {
            ResultGreedyCyclePoint firstGraphBestInsertPoint = null;
            if (firstNoElements < sizeGraph / 2)
                firstGraphBestInsertPoint = calculatePointToNewTraceWithSmallestCost(firstGraph, costMatrix, availablePoints);

            ResultGreedyCyclePoint secondGraphBestInsertPoint = null;
            if (secondNoElements < sizeGraph / 2)
                secondGraphBestInsertPoint = calculatePointToNewTraceWithSmallestCost(secondGraph, costMatrix, availablePoints);


            if (firstGraphBestInsertPoint == null) {
                if (secondGraphBestInsertPoint == null) {
                    throw new IllegalStateException();
                } else {
                    insertElementToGraph(secondGraphBestInsertPoint.getIndexToInsert(), secondGraphBestInsertPoint.getValueToInsert(), secondGraph, availablePoints);
                    secondNoElements++;
                }
            } else {
                if (secondGraphBestInsertPoint == null) {
                    firstNoElements++;
                    insertElementToGraph(firstGraphBestInsertPoint.getIndexToInsert(), firstGraphBestInsertPoint.getValueToInsert(), firstGraph, availablePoints);
                } else {
                    if (firstGraphBestInsertPoint.getInsertionCost() < secondGraphBestInsertPoint.getInsertionCost()) {
                        firstNoElements++;
                        insertElementToGraph(firstGraphBestInsertPoint.getIndexToInsert(), firstGraphBestInsertPoint.getValueToInsert(), firstGraph, availablePoints);
                    } else {
                        insertElementToGraph(secondGraphBestInsertPoint.getIndexToInsert(), secondGraphBestInsertPoint.getValueToInsert(), secondGraph, availablePoints);
                        secondNoElements++;
                    }
                }

            }
        }

        return new ResultGraphs(firstGraph, secondGraph);
    }

    private ResultGreedyCyclePoint calculatePointToNewTraceWithSmallestCost(List<Integer> graph, long[][] costMatrix, List<Integer> availablePoints){
        int indexToInsert = -1;
        int valueToInsert = -1;
        long insertionCost = Long.MAX_VALUE;

        int graphLastElement = graph.get(graph.size() - 1);
        int graphFirstElement = graph.get(0);


        for(int availablePoint: availablePoints){
            if(costMatrix[graphFirstElement][availablePoint] + costMatrix[availablePoint][graphLastElement] - costMatrix[graphFirstElement][graphLastElement] < insertionCost){
                indexToInsert = 0;
                valueToInsert = availablePoint;
                insertionCost = costMatrix[graphFirstElement][availablePoint] + costMatrix[availablePoint][graphLastElement] - costMatrix[graphFirstElement][graphLastElement];
            }
        }


        for(int i=0; i < graph.size()-1; i++){
            int graphPoint1 = graph.get(i);
            int graphPoint2 = graph.get(i+1);
            for(int availablePoint: availablePoints){
                if(costMatrix[graphPoint1][availablePoint] + costMatrix[availablePoint][graphPoint2] - costMatrix[graphPoint1][graphPoint2] < insertionCost){
                    insertionCost = costMatrix[graphPoint1][availablePoint] + costMatrix[availablePoint][graphPoint2] - costMatrix[graphPoint1][graphPoint2];
                    indexToInsert = i+1;
                    valueToInsert = availablePoint;
                }
            }

        }

        if(indexToInsert == -1 || valueToInsert == -1){
            throw new IllegalStateException();
        }

        return new ResultGreedyCyclePoint(indexToInsert, valueToInsert, insertionCost);
    }

    private List<Integer> createThreePointsGraph(int firstPoint, long[][] costMatrix, List<Integer> availablePoints){
        List<Integer> returnGraph = new LinkedList<>();
        addElementToGraph(firstPoint, returnGraph, availablePoints);

        int nearestPointToFirst = minDistancePoint(firstPoint, costMatrix[firstPoint], availablePoints);
        addElementToGraph(nearestPointToFirst, returnGraph, availablePoints);

        long minimumCost = Long.MAX_VALUE;
        int thirdPoint = -1;
        for(Integer availablePoint: availablePoints){
            if(costMatrix[firstPoint][availablePoint] + costMatrix[availablePoint][nearestPointToFirst] < minimumCost){
                minimumCost = costMatrix[firstPoint][availablePoint] + costMatrix[availablePoint][nearestPointToFirst];
                thirdPoint = availablePoint;
            }
        }
        if(thirdPoint == -1){
            throw new IllegalStateException();
        }

        addElementToGraph(thirdPoint, returnGraph, availablePoints);


        return returnGraph;
    }

    private void addElementToGraph(int element, List<Integer> graph, List<Integer> availablePoints){
        graph.add(element);
        availablePoints.remove(Integer.valueOf(element));
    }

    private void insertElementToGraph(int index, int element, List<Integer> graph, List<Integer> availablePoints){
        graph.add(index, element);
        availablePoints.remove(Integer.valueOf(element));
    }


    private int minDistancePoint(int referencePoint, long[] costVector, List<Integer> availablePoints){
        long minValue = Long.MAX_VALUE;
        int nearestPoint = -1;
        for(int availablePoint: availablePoints){
            if(costVector[availablePoint] < minValue && referencePoint != availablePoint){
                minValue = costVector[availablePoint];
                nearestPoint = availablePoint;
            }
        }
        if(nearestPoint == -1){
            throw new IllegalStateException();
        }
        return nearestPoint;
    }
}
