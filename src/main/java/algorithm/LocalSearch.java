package algorithm;

import result.ResultGraphs;

import java.util.Collections;
import java.util.List;

public abstract class LocalSearch implements Algorithm {


    protected ResultGraphs calculationForEachGraph(List<Integer> firstGraph, List<Integer> secondGraph, long[][] costMatrix, int sizeGraph){

        long firstProfit;
        long secondProfit;
        for(int i=0; i<sizeGraph/2-2; i++){
            long firstGraphProfit = 0;
            int firstGraphFirstPoint = -1;
            int firstGraphSecondPoint = -1;

            long secondGraphProfit = 0;
            int secondGraphFirstPoint = -1;
            int secondGraphSecondPoint = -1;

            for(int j=i+3; j<sizeGraph/2; j++){
                firstProfit = calculateProfitOfEdgeChangeInOneGraph(i, j, firstGraph, costMatrix);
                if(firstProfit > firstGraphProfit){
                    firstGraphProfit = firstProfit;
                    firstGraphFirstPoint = i;
                    firstGraphSecondPoint = j;
                }

                secondProfit = calculateProfitOfEdgeChangeInOneGraph(i, j, secondGraph, costMatrix);
                if(secondProfit > secondGraphProfit){
                    secondGraphProfit = secondProfit;
                    secondGraphFirstPoint = i;
                    secondGraphSecondPoint = j;
                }
            }

            if(firstGraphFirstPoint != -1 && firstGraphSecondPoint != -1){
                swapEdges(firstGraphFirstPoint, firstGraphSecondPoint, firstGraph);
            }

            if(secondGraphFirstPoint != -1 && secondGraphSecondPoint != -1){
                swapEdges(secondGraphFirstPoint, secondGraphSecondPoint, secondGraph);
            }
        }
        return new ResultGraphs(firstGraph, secondGraph);
    }

    protected long calculateProfitOfEdgeChangeInOneGraph(int firstPoint, int secondPoint, List<Integer> graph, long[][] costMatrix){
        int firstPointValue = graph.get(firstPoint);
        int firstPointPlusOneValue = graph.get(firstPoint+1);
        int secondPointValue = graph.get(secondPoint);
        int secondPointMinusOneValue = graph.get(secondPoint-1);

        return (costMatrix[firstPointValue][firstPointPlusOneValue] + costMatrix[secondPointMinusOneValue][secondPointValue]
                - costMatrix[firstPointValue][secondPointMinusOneValue] - costMatrix[firstPointPlusOneValue][secondPointValue]);
    }

    protected void swapEdges(int firstPoint, int secondPoint, List<Integer> graph){
        firstPoint++;
        secondPoint--;
        while(firstPoint < secondPoint){
            Collections.swap(graph, firstPoint, secondPoint);
            firstPoint++;
            secondPoint--;
        }
    }
}
