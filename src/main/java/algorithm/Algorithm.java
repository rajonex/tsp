package algorithm;

import result.ResultGraphs;

public interface Algorithm {
    ResultGraphs calculate(int sizeGraph, long[][] costMatrix);
}
