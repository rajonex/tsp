package algorithm;

import result.ResultGraphs;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class LocalSearchEdgeChange extends LocalSearch {

    private List<Integer> firstGraph;
    private List<Integer> secondGraph;

    public LocalSearchEdgeChange(List<Integer> firstGraph, List<Integer> secondGraph) {
        this.firstGraph = new LinkedList<>(firstGraph);
        this.secondGraph = new LinkedList<>(secondGraph);
    }

    public LocalSearchEdgeChange(ResultGraphs resultGraphs){
        this.firstGraph = new LinkedList<>(resultGraphs.getFirstGraph());
        this.secondGraph = new LinkedList<>(resultGraphs.getSecondGraph());
    }

    @Override
    public ResultGraphs calculate(int sizeGraph, long[][] costMatrix) {
        return calculationForEachGraph(firstGraph, secondGraph, costMatrix, sizeGraph);
    }


}
