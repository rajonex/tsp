package XML;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class KroParser {
    public static long[][] parseKroX(String filename, int size) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        File file = new File(filename);
        Document document = builder.parse(file);
        Element element = document.getDocumentElement();
        NodeList graphElements = element.getElementsByTagName("graph");
        Element graphElement = (Element) graphElements.item(0);
        NodeList graphPoints = graphElement.getChildNodes();

        long[][] costMatrix = new long[size][size];
        for (int i = 0; i < graphPoints.getLength(); i++) {
            Node point = graphPoints.item(i);
            if (point.getNodeType() == Node.ELEMENT_NODE) {
                Element iterElement = (Element) point;
                NodeList graphDistances = iterElement.getChildNodes();
//                    System.out.println("::::::::: " + (i-1)/2 + " ::::::::::::::::");
                for (int j = 0; j < graphDistances.getLength(); j++) {
                    Node distance = graphDistances.item(j);
                    if (distance.getNodeType() == Node.ELEMENT_NODE) {
                        Element distElement = (Element) distance;
                        costMatrix[((int) ((i - 1) / 2))][Integer.valueOf(distElement.getTextContent())] = Math.round(Double.valueOf(distElement.getAttribute("cost")));
                    }

                }
                costMatrix[((int) ((i - 1) / 2))][((int) ((i - 1) / 2))] = 0L;

            }
//            else {
//                System.out.println("ElementType " + point.getNodeType());
//            }
        }
        return costMatrix;
    }
}
