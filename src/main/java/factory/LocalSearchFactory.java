package factory;

import algorithm.Algorithm;
import algorithm.LocalSearch;
import algorithm.LocalSearchBetweenGraphs;
import algorithm.LocalSearchEdgeChange;
import result.ResultGraphs;

import java.util.List;

public class LocalSearchFactory {

    public static LocalSearch createLocalSearchAlgorithm(boolean type, List<Integer> firstGraph, List<Integer> secondGraph){
        if(type)
            return new LocalSearchBetweenGraphs(firstGraph, secondGraph);
        else
            return new LocalSearchEdgeChange(firstGraph, secondGraph);
    }

    public static LocalSearch createLocalSearchAlgorithm(boolean type, ResultGraphs resultGraphs){
        if(type)
            return new LocalSearchBetweenGraphs(resultGraphs);
        else
            return new LocalSearchEdgeChange(resultGraphs);
    }
}
