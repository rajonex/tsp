package utils;

import result.Edge;
import result.ResultGraphs;
import result.ResultQualitySimilarity;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.LinkedList;
import java.util.List;

public class TSPUtils {
    public static double[][] convertLongToDouble(long[][] matrix){
        int matrixSize = matrix.length;
        double[][] doubleMatrix = new double[matrixSize][matrixSize];
        for(int i=0; i<matrixSize; i++){
            for(int j=0; j<matrixSize; j++){
                doubleMatrix[i][j] = matrix[i][j];
            }
        }

        return doubleMatrix;
    }

    public static List<Edge> convertResultGraphsToEdges(ResultGraphs resultGraphs){
        List<Edge> resultEdges = new LinkedList<>();
        List<Integer> firstGraph = resultGraphs.getFirstGraph();
        resultEdges.add(new Edge(firstGraph.get(firstGraph.size()-1), firstGraph.get(0)));
        for(int i=0; i<firstGraph.size()-1; i++){
            resultEdges.add(new Edge(firstGraph.get(i), firstGraph.get(i+1)));
        }

        List<Integer> secondGraph = resultGraphs.getSecondGraph();
        resultEdges.add(new Edge(secondGraph.get(secondGraph.size()-1), secondGraph.get(0)));
        for(int i=0; i<secondGraph.size()-1; i++){
            resultEdges.add(new Edge(secondGraph.get(i), secondGraph.get(i+1)));
        }

        return resultEdges;
    }

    public static void savePointsMatrix(double[][] points, String filename){
        int n = points[0].length;
        try(
                FileWriter fileWriter = new FileWriter(filename);
                BufferedWriter writer = new BufferedWriter(fileWriter);
        ) {
            for(int i=0; i<n; i++) {  // output all coordinates
                writer.write(i + "," + points[0][i] + "," + points[1][i] + "\n");
            }

        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveGraphPoints(List<Integer> points, String filename){

        try(
                FileWriter fileWriter = new FileWriter(filename);
                BufferedWriter writer = new BufferedWriter(fileWriter);
        ) {
            for(int point: points) {  // output all coordinates
                writer.write(point + ",");
            }

        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveResultInformation(String filename, Duration bestDuration, long bestScore, long worstScore, long averageScore, Duration averageDuration, int numberOfIterations ){
        try(
                FileWriter fileWriter = new FileWriter(filename);
                BufferedWriter writer = new BufferedWriter(fileWriter);
        ) {
            writer.write("Czas wykonania najlepszego rozwiązania: " + bestDuration.toNanos() + " ns");
            writer.write("\nWynik najlepszego rozwiązania: " + bestScore);
            writer.write("\nWynik najgorszego rozwiązania: " + worstScore);
            writer.write("\nŚredni wynik rozwiązania: " + averageScore);
            writer.write("\nŚredni czas wykonania: " + averageDuration.toNanos() + " ns");
            writer.write("\nLiczba iteracji: " + numberOfIterations);

        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveCorrelation(List<ResultQualitySimilarity> corr, String filename){
        try(
                FileWriter fileWriter = new FileWriter(filename);
                BufferedWriter writer = new BufferedWriter(fileWriter);
        ) {
            for(ResultQualitySimilarity qualitySimilarity: corr) {  // output all coordinates
                writer.write(qualitySimilarity.getSimilarity() + "," + qualitySimilarity.getQuality() + "\n");
            }

        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
