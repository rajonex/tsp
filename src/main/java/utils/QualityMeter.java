package utils;

import result.Edge;
import result.ResultGraphs;
import result.ResultQualitySimilarity;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class QualityMeter {
    public static List<ResultQualitySimilarity> correlationToBest(List<ResultGraphs> localOptimums, long[][] costMatrix){
        List<ResultQualitySimilarity> result = new LinkedList<>();
        List<Double> qualities = getQualities(localOptimums, costMatrix);
        List<List<Edge>> edges = getEdges(localOptimums);
        List<Edge> bestResultGraphsEdges = edges.get(qualities.indexOf(Collections.max(qualities)));
        List<Integer> similarities = getSimilaritiesToOne(edges, bestResultGraphsEdges);

        for(int i=0; i<localOptimums.size(); i++){
            result.add(new ResultQualitySimilarity(qualities.get(i), similarities.get(i)));
        }

        return result;
    }

    public static List<ResultQualitySimilarity> correlationToEachOther(List<ResultGraphs> localOptimums, long[][] costMatrix){
        List<ResultQualitySimilarity> result = new LinkedList<>();
        List<Double> qualities = getQualities(localOptimums, costMatrix);
        List<List<Edge>> edges = getEdges(localOptimums);
        List<Double> similarities = getSimilaritiesToAll(edges);

        for(int i=0; i<localOptimums.size(); i++){
            result.add(new ResultQualitySimilarity(qualities.get(i), similarities.get(i)));
        }

        return result;
    }

    private static List<Double> getQualities(List<ResultGraphs> localOptimums, long[][] costMatrix){
        List<Double> resultQualities = new LinkedList<>();
        ResultCalculation resultCalculation = new ResultCalculation(costMatrix);

        for(ResultGraphs iterGraphs: localOptimums){
            resultQualities.add(1.0 / resultCalculation.calculateWholeDistanceOfTwoGraphs(iterGraphs.getFirstGraph(), iterGraphs.getSecondGraph()));
        }

        return resultQualities;
    }

    private static List<List<Edge>> getEdges(List<ResultGraphs> localOptimums){
        List<List<Edge>> resultEdges = new LinkedList<>();
        for(ResultGraphs localOptimum: localOptimums){
            resultEdges.add(TSPUtils.convertResultGraphsToEdges(localOptimum));
        }
        return resultEdges;
    }

    private static List<Double> getSimilaritiesToAll(List<List<Edge>> localOptimumsEdges){
        List<Double> resultSimilarities = new LinkedList<>();
        int elementsNumber = localOptimumsEdges.size();
        for(int i = 0; i<elementsNumber; i++){
            int iterSimilarity = 0;
            for(int j = 0; j<elementsNumber; j++){
                if(i==j)
                    continue;

                List<Edge> iterEdges = localOptimumsEdges.get(i);
                List<Edge> otherEdges = localOptimumsEdges.get(j);

                iterSimilarity += calculcateSimilarity(iterEdges, otherEdges);
            }
            resultSimilarities.add((double)iterSimilarity/(elementsNumber-1));
        }

        return resultSimilarities;
    }

    private static List<Integer> getSimilaritiesToOne(List<List<Edge>> localOptimumsEdges, List<Edge> bestSolutionEdges){
        List<Integer> resultSimilarities = new LinkedList<>();
        for(List<Edge> localOptimum: localOptimumsEdges){
            resultSimilarities.add(calculcateSimilarity(localOptimum, bestSolutionEdges));
        }

        return resultSimilarities;
    }

    private static int calculcateSimilarity(List<Edge> localOptimumEdges, List<Edge> bestSolutionEdges){
        int result = 0;
        for(Edge localOptimumEdge: localOptimumEdges){
            if(bestSolutionEdges.contains(localOptimumEdge)){
                result++;
            }
        }
        return result;
    }
}
