package utils;

import result.ResultGraphs;

import java.util.List;

public class ResultCalculation {

    private long[][] costMatrix;

    public ResultCalculation(long[][] costMatrix) {
        this.costMatrix = costMatrix;
    }

    public long calculateWholeDistance(List<Integer> graphPoints){
        long result = 0;
        int n = graphPoints.size();
        int firstPoint = graphPoints.get(n-1);
        int secondPoint;
        for(int i=0; i<n; i++){
            secondPoint = graphPoints.get(i);
            result += costMatrix[firstPoint][secondPoint];
            firstPoint = secondPoint;
        }

        return result;
    }

    public long calculateWholeDistanceOfTwoGraphs(List<Integer> firstGraph, List<Integer> secondGraph){
        return (calculateWholeDistance(firstGraph) + calculateWholeDistance(secondGraph));
    }

    public long calculateWholeDistanceOfTwoGraphs(ResultGraphs resultGraphs){
        return calculateWholeDistanceOfTwoGraphs(resultGraphs.getFirstGraph(), resultGraphs.getSecondGraph());
    }
}
