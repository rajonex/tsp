package result;

public class ResultGreedyCyclePoint {
    private int indexToInsert;
    private int valueToInsert;
    private long insertionCost;

    public ResultGreedyCyclePoint(int indexToInsert, int valueToInsert, long insertionCost) {
        this.indexToInsert = indexToInsert;
        this.valueToInsert = valueToInsert;
        this.insertionCost = insertionCost;
    }

    public int getIndexToInsert() {
        return indexToInsert;
    }

    public int getValueToInsert() {
        return valueToInsert;
    }

    public long getInsertionCost() {
        return insertionCost;
    }
}
