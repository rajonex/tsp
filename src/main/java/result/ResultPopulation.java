package result;

import java.util.LinkedList;
import java.util.List;

public class ResultPopulation {
    private List<ResultGraphs> population;
     private List<Long> populationCosts;

    public ResultPopulation(List<ResultGraphs> population, List<Long> populationCosts) {
        this.population = population;
        this.populationCosts = populationCosts;
    }

    public List<ResultGraphs> getPopulation() {
        return population;
    }

    public List<Long> getPopulationCosts() {
        return populationCosts;
    }
}
