package result;

import java.util.Objects;

public class Edge {
    private int firstPoint;
    private int secondPoint;

    public Edge(int firstPoint, int secondPoint) {
        this.firstPoint = firstPoint;
        this.secondPoint = secondPoint;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge edge = (Edge) o;
        return (firstPoint == edge.firstPoint && secondPoint == edge.secondPoint)
                || (firstPoint == edge.secondPoint && secondPoint == edge.firstPoint);
    }

}
