package result;

public class ResultQualitySimilarity {
    private double quality;
    private double similarity;

    public ResultQualitySimilarity(double quality, double similarity) {
        this.quality = quality;
        this.similarity = similarity;
    }

    public double getQuality() {
        return quality;
    }

    public double getSimilarity() {
        return similarity;
    }
}
