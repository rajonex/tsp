package result;

import java.util.LinkedList;
import java.util.List;

public class ResultGraphs {
    List<Integer> firstGraph;
    List<Integer> secondGraph;

    public ResultGraphs(List<Integer> firstGraph, List<Integer> secondGraph) {
        this.firstGraph = firstGraph;
        this.secondGraph = secondGraph;
    }
//
//    public ResultGraphs(ResultGraphs copy){
//        this.firstGraph = new LinkedList<>(copy.getFirstGraph());
//        this.secondGraph = new LinkedList<>(copy.getSecondGraph());
//    }

    public List<Integer> getFirstGraph() {
        return firstGraph;
    }

    public List<Integer> getSecondGraph() {
        return secondGraph;
    }
}
