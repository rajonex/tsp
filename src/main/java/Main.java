import XML.KroParser;
import algorithm.*;
import mdsj.MDSJ;
import result.ResultGraphs;
import result.ResultQualitySimilarity;
import utils.QualityMeter;
import utils.ResultCalculation;
import utils.TSPUtils;

import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        int sizeGraph = 100;
        long[][] costMatrix = null;
        try {
            costMatrix = KroParser.parseKroX("src/main/resources/kroA100.xml", sizeGraph);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ResultCalculation resultCalculation = new ResultCalculation(costMatrix);

//        String folderName = "Local_Search_Rand";
//
//        Duration bestDuration = Duration.ZERO;
//        long bestScore = Long.MAX_VALUE;
//        ResultGraphs bestResult = null;
//        long worstScore = Long.MIN_VALUE;
//        long averageScore = 0;
//        Duration averageDuration = Duration.ZERO;
//        int numberOfIterations = 100;
//        long sumDurationInNanos = 0;
//
//
//        for(int i=0; i<numberOfIterations; i++){
//            long startTime = System.nanoTime();
//            Algorithm algorithm = new RandTSP();
//            ResultGraphs resultGraphsPart = algorithm.calculate(sizeGraph, costMatrix);
//            Algorithm localSearch = new LocalSearchEdgeChange(resultGraphsPart);
//            ResultGraphs resultGraphs = localSearch.calculate(sizeGraph, costMatrix);
//
//            long durationInNanos = System.nanoTime() - startTime;
//            Duration duration = Duration.ofNanos(durationInNanos);
//            sumDurationInNanos += durationInNanos;
//
//            long score = resultCalculation.calculateWholeDistanceOfTwoGraphs(resultGraphs);
//            if(bestScore > score){
//                bestResult = resultGraphs;
//                bestScore = score;
//                bestDuration = duration;
//            }
//
//            if(worstScore < score){
//                worstScore = score;
//            }
//
//            averageScore += score;
//        }
//        averageScore /= numberOfIterations;
//        averageDuration = Duration.ofNanos(sumDurationInNanos/numberOfIterations);
//
//
//
//
//        File dir = new File("C:\\Octave_workspace\\" + folderName);
//        if(!(dir.exists() && dir.isDirectory())){
//            dir.mkdir();
//        }
//
//        TSPUtils.saveResultInformation("C:\\Octave_workspace\\" + folderName + "\\informations.txt", bestDuration, bestScore, worstScore, averageScore, averageDuration, numberOfIterations);
//
//        TSPUtils.saveGraphPoints(bestResult.getFirstGraph(), "C:\\Octave_workspace\\" + folderName + "\\first_graph.txt");
//        TSPUtils.saveGraphPoints(bestResult.getSecondGraph(), "C:\\Octave_workspace\\" + folderName + "\\second_graph.txt");

////        Algorithm algorithm = new GreedyCycle();
        Algorithm algorithm = new NearestNeighbor();
        ResultGraphs resultGraphs = algorithm.calculate(sizeGraph, costMatrix);
        List<Integer> firstGraph = resultGraphs.getFirstGraph();
        List<Integer> secondGraph = resultGraphs.getSecondGraph();

        Algorithm wrapper = new LocalSearchEdgeChange(firstGraph, secondGraph);
        ResultGraphs resultWrapper = wrapper.calculate(sizeGraph, costMatrix);
        List<Integer> firstGraphModified = resultWrapper.getFirstGraph();
        List<Integer> secondGraphModified = resultWrapper.getSecondGraph();

        Algorithm multipleLocalSearchWrapper = new MultipleStartLocalSearchEdgeChange(true, algorithm, 100);
        Instant startTime = Instant.now();
        ResultGraphs resultWrapperTwice = multipleLocalSearchWrapper.calculate(sizeGraph, costMatrix);
        Duration duration = Duration.between(startTime, Instant.now());
        List<Integer> firstGraphMultipleStart = resultWrapperTwice.getFirstGraph();
        List<Integer> secondGraphMultipleStart = resultWrapperTwice.getSecondGraph();

        Algorithm localSearchBetweenGraphs = new LocalSearchBetweenGraphs(firstGraph, secondGraph);
        ResultGraphs resultLSBG = localSearchBetweenGraphs.calculate(sizeGraph, costMatrix);
        List<Integer> firstGraphLSBG = resultLSBG.getFirstGraph();
        List<Integer> secondGraphLSBG= resultLSBG.getSecondGraph();



        Algorithm iterative = new IterativeLocalSearchEdgeChange(true, firstGraph, secondGraph, duration);
//        Algorithm iterative = new IterativeLocalSearchEdgeChange(firstGraph, secondGraph, Duration.ofSeconds(4));
        ResultGraphs resultGraphsIterative = iterative.calculate(sizeGraph, costMatrix);
        List<Integer> firstGraphIterative = resultGraphsIterative.getFirstGraph();
        List<Integer> secondGraphIterative = resultGraphsIterative.getSecondGraph();

//        Algorithm evolutionary = new EvolutionaryAlgorithm(algorithm, 20, Duration.ofSeconds(3));
        Algorithm evolutionary = new EvolutionaryAlgorithm(true, algorithm, 20, duration);
        ResultGraphs resultGraphsEvolutionary = evolutionary.calculate(sizeGraph, costMatrix);
        List<Integer> firstGraphEvolutionary = resultGraphsEvolutionary.getFirstGraph();
        List<Integer> secondGraphEvolutionary = resultGraphsEvolutionary.getSecondGraph();


//
//        double[][] distances = TSPUtils.convertLongToDouble(costMatrix);
//        double[][] points = MDSJ.classicalScaling(distances);


//        TSPUtils.savePointsMatrix(points, "C:\\Octave_workspace\\matrix.txt");

//        TSPUtils.saveGraphPoints(firstGraph, "C:\\workspace_octave\\first_graph.txt");
//        TSPUtils.saveGraphPoints(secondGraph, "C:\\workspace_octave\\second_graph.txt");
//        TSPUtils.saveGraphPoints(firstGraphModified, "C:\\workspace_octave\\first_graph_modified.txt");
//        TSPUtils.saveGraphPoints(secondGraphModified, "C:\\workspace_octave\\second_graph_modified.txt");
//        TSPUtils.saveGraphPoints(firstGraphIterative, "C:\\workspace_octave\\first_graph_modified_iter.txt");
//        TSPUtils.saveGraphPoints(secondGraphIterative, "C:\\workspace_octave\\second_graph_modified_iter.txt");
//        TSPUtils.saveGraphPoints(firstGraphEvolutionary, "C:\\workspace_octave\\first_graph_evo.txt");
//        TSPUtils.saveGraphPoints(secondGraphEvolutionary, "C:\\workspace_octave\\second_graph_evo.txt");

//
//        System.out.println(resultCalculation.calculateWholeDistance(firstGraph));
//        System.out.println(resultCalculation.calculateWholeDistance(secondGraph));

        System.out.println("Old LS");
        System.out.println(resultCalculation.calculateWholeDistance(firstGraphModified));
        System.out.println(resultCalculation.calculateWholeDistance(secondGraphModified));

        System.out.println("MSLS");
        System.out.println(resultCalculation.calculateWholeDistance(firstGraphMultipleStart));
        System.out.println(resultCalculation.calculateWholeDistance(secondGraphMultipleStart));

        System.out.println("New LS");
        System.out.println(resultCalculation.calculateWholeDistance(firstGraphLSBG));
        System.out.println(resultCalculation.calculateWholeDistance(secondGraphLSBG));


        System.out.println("Iterative");
        System.out.println(resultCalculation.calculateWholeDistance(firstGraphIterative));
        System.out.println(resultCalculation.calculateWholeDistance(secondGraphIterative));

        System.out.println("Evo");
        System.out.println(resultCalculation.calculateWholeDistance(firstGraphEvolutionary));
        System.out.println(resultCalculation.calculateWholeDistance(secondGraphEvolutionary));

//        System.out.println(duration);
//
//
//
//
//        // Task 4
//
//        int optimumsNumber = 100;
//        List<ResultGraphs> localOptimums = new LinkedList<>();
//        for(int i=0; i<optimumsNumber; i++){
//            Algorithm nn = new NearestNeighbor();
//            ResultGraphs resultGraphsMulti = nn.calculate(sizeGraph, costMatrix);
//            List<Integer> firstGraphMulti = resultGraphsMulti.getFirstGraph();
//            List<Integer> secondGraphMulti = resultGraphsMulti.getSecondGraph();
//
//            Algorithm localSearch = new LocalSearchEdgeChange(firstGraphMulti, secondGraphMulti);
//            localOptimums.add(localSearch.calculate(sizeGraph, costMatrix));
//        }
//
//        List<ResultQualitySimilarity> corrQualitySimilarity = QualityMeter.correlationToEachOther(localOptimums, costMatrix);
//        TSPUtils.saveCorrelation(corrQualitySimilarity, "C:\\workspace_octave\\point4.txt");


        // KK LAYOUT
        // https://www.programcreek.com/java-api-examples/?code=marcvanzee/mdp-plan-revision/mdp-plan-revision-master/src/UNUSED/JUNGsamples/VisualizationImageServerDemo.java


    }
}
